#!/bin/bash

# Função para exibir o menu
exibir_menu() {
    echo "Escolha uma opção:"
    echo "1. Opção 1 - Realizar tarefa 1"
    echo "2. Opção 2 - Realizar tarefa 2"
    echo "3. Opção 3 - Realizar tarefa 3"
    echo "0. Sair"
    echo
}

# Função para realizar a Tarefa 1
realizar_tarefa1() {
    echo "Executando Tarefa 1..."
    # Insira o código para a Tarefa 1 aqui
}

# Função para realizar a Tarefa 2
realizar_tarefa2() {
    echo "Executando Tarefa 2..."
    # Insira o código para a Tarefa 2 aqui
}

# Função para realizar a Tarefa 3
realizar_tarefa3() {
    echo "Executando Tarefa 3..."
    # Insira o código para a Tarefa 3 aqui
}

# Loop principal do programa
while true; do
    exibir_menu
    read -p "Digite o número da opção desejada: " opcao
    echo
    
    case $opcao in
        1)
            realizar_tarefa1
            ;;
        2)
            realizar_tarefa2
            ;;
        3)
            realizar_tarefa3
            ;;
        0)
            echo "Saindo..."
            break
            ;;
        *)
            echo "Opção inválida! Tente novamente."
            ;;
    esac
    
    echo
done
