#!/bin/bash

SERVICE_USER=xxx
PRIVATE_KEY=private_key
SERVER=carlosdiegoc.com
LOGFILE=/var/log/$(date + %d-%m-%Y)-file_transfer_feed.log

function_transfer_files(){
    ls $SOURCE_DIR/ | grep .xls # Lista todos os arquivos com extensão xml dentro do diretório.
    ret=$? # Armazena o valor de retorno do comando executado anteriormente, caso existam arquivos o valor será 0.
    echo $ret # Ecoa o valor da variável que foi atribuiado acima.
    if [ $ret -eq 0 ]; then # No caso de existirem arquivos, executa os comandos abaixo.
        echo "Files ready to be transfered" >> $LOGFILE 2>&1 #Ecoa a mensagem dentro do arquivo de log e a saída de erro pro devnull.
        for list_files in "${files[@]}"; do #Percorre a lista de arquivos que é passada em outra função.
            scp -i ${PRIVATE_KEY} -p ${SOURCE_DIR}/$list_files ${SERVICE_USER}@${SERVER}:${DESTINATION_DIR} && echo $list_files successfully copied to ${DESTINATION_DIR} >> $LOGFILE 2>&1 #Comando para transferencia de arquivos utilizando protocolo scp.
            done
        echo To download log files, acess $LOGFILE
    else
        echo "Not found files to transfer" >> $LOGFILE 2>&1
        echo $SOURCE_DIR/$list_files >> $LOGFILE 2>&1
        echo Access $LOGFILE and check log!
    fi
}

function_move_files(){ # Função para mover os arquivos que já foram copiados pro outro servidor para um diretório de arquivamento. 
    ls $SOURCE_DIR/ | grep .xls
    ret=$?
    echo $ret
    if [ $ret -eq 0 ]; then
        echo "Files ready to be moved" >> $LOGFILE 2>&1
        for list_files in "${files[@]}"; do
            mv -v ${SOURCE_DIR}/$list_files ${SURCE_DIR_SEND} && echo $list_files successfully moved to ${SURCE_DIR_SEND} >> $LOGFILE 2>&1
            done
        echo To download log files, acess $LOGFILE
    else
        echo "Not found files to move" >> $LOGFILE 2>&1
        echo $SOURCE_DIR/$file >> $LOGFILE 2>&1
        echo Access $LOGFILE and check log!
    fi
}

function_ssh_connection_test(){ # Função para testar conectividade ssh.
    ssh -i ${PRIVATE_KEY} -q ${SERVICE_USER}@${SERVER} exit # Estabelece sessão ssh apontando o caminho completo da chave privada de forma silenciosa (-q quiet mode), ou seja, com uma saída mais limpa.
    ret=$?
    echo $ret
    if [ $ret -eq 0 ]; then
        echo "Connection established to server." >> $LOGFILE 2>&1
    else
        echo "Can not connect to server." >> $LOGFILE 2>&1
    fi
}



PS3='Please enter your choice to tranfer files: '
options=("Transfer files to feed base01" "Transfer files to feed base02" "Transfer files to feed base03" "Quit")
echo ""
echo "---------"
echo "Select options 1 through 3 to send the files to your respective base feeds or press 4 to exit."
echo "---------"
echo ""

select opt in "${options[@]}"
do
    case $opt in 
        "Transfer files to feed base01")
            function_ssh_connection_test # Chama a função de teste de conexão definida acima.
            # Constantes que irão variar de acordo com o case.
            SOURCE_DIR=/app 
            SOURCE_DIR_SEND=/app/snd
            DESTINATION_DIR=/db/cloud
            echo "Transfering files for base01" | tee -a $LOGFILE # Exibe a saída no terminal e grava no arquivo de log, o -a indica que haverá uma adição de conteúdo e não uma substituição.
            echo "Enter the path a log transmission file"
            read logfilepath # Solicita ao usuário o caminho para o arquivo de log que será lido.
            chmod 444 $logfilepath # O arquivo terá permissões de leitura para todos os usuários, mas não terá permissões de gravação ou execução.
            file=$(cat $logfilepath) # Atribui o conteúdo do arquivo de log a variável "file".
            files=("${file}") # A sintaxe (${file}) é usada para converter o conteúdo em um array, onde cada linha do arquivo se torna um elemento do array.
            chmod 644 $logfilepath # O arquivo terá permissões de leitura e gravação para o proprietário do arquivo, mas apenas permissões de leitura para outros usuários.
            function_transfer_files # Chama a função de transferência de arquivos definida acima.
            function_move_files # Chama a função para mover os arquivos definida acima.
            mv -v ${logfilepath} ${SOURCE_DIR_SEND} && echo $logfilepath successfully moved to ${SOURCE_DIR_SEND} >> $LOGFILE 2>&1 # O -v indica --verbose, informações mais detalhadas.
            # É importante ressaltar que o echo acima só será executado caso o comando mv seja bem sucedido (&&). Este comando move o arquivo de log da pasta de origem para a pasta de arquivamento.
            ;; # Aqui encerra o escopo do primeiro case.
        "Transfer files to feed base02")
            function_ssh_connection_test
            SOURCE_DIR=/app
            SOURCE_DIR_SEND=/app/snd
            DESTINATION_DIR=/db/cloud
            echo "Transfering files to feed base02" | tee -a $LOGFILE
            echo "Enter the path a log transmission file"
            read logfilepath
            chmod 444 $logfilepath
            file=`cat $logfilepath`
            files=(${file})
            chmod 644 $logfilepath
            function_transfer_files
            function_move_files
            mv -v ${logfilepath} ${SOURCE_DIR_SEND} && echo $logfilepath successfully moved to ${SOURCE_DIR_SEND} >> $LOGFILE 2>&1
            ;;
        "Transfer files to feed base03")
            function_ssh_connection_test
            SOURCE_DIR=/app
            SOURCE_DIR_SEND=/app/snd
            DESTINATION_DIR=/db/cloud
            echo "Transfering files to feed base03" | tee -a $LOGFILE
            echo "Enter the path a log transmission file"
            read logfilepath
            chmod 444 $logfilepath
            file=`cat $logfilepath`
            files=(${file})
            chmod 644 $logfilepath
            function_transfer_files
            function_move_files
            mv -v ${logfilepath} ${SOURCE_DIR_SEND} && echo $logfilepath successfully moved to ${SOURCE_DIR_SEND} >> $LOGFILE 2>&1
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY" | tee -a $LOGFILE ;;
    esac
done

